const {readFileSync, writeFileSync} = require('node:fs');

const {contextBridge} = require('electron')
const {app} = require("electron")

function readFile(path){
    return readFileSync(path,{encoding:"utf-8"});
}

function writeFile(path,msg){
    return writeFileSync(path,msg,{
        encoding:"utf-8",
        flag:"w"
    });
}
contextBridge.exposeInMainWorld("writer",{writeFile:writeFile});
contextBridge.exposeInMainWorld("reader",{readFile:readFile});


