const { app, BrowserWindow } = require('electron')
const path = require('node:path')

const createWindow = () => {
    const win = new BrowserWindow({
      width: 1920,
      height: 1080,
      frame:false,
      webPreferences: {
        preload: path.join(__dirname, 'preload.js'),
        nodeIntegration: true,
        contextIsolation: true,
        enableRemoteModule: true
      }
    })
  win.loadFile('index.html')
  win.maximize()
}

// const createWindow = () => {
//   const win = new BrowserWindow({
//     width: 900,
//     height: 800,
//     // frame:false,
//     webPreferences: {
//       preload: path.join(__dirname, 'preload.js'),
//       nodeIntegration: true,
//       contextIsolation: true,
//       enableRemoteModule: true
//     }
//   })
// win.loadFile('index.html')
// // win.maximize()
// }


  app.whenReady().then(() => {
    createWindow()
  
    app.on('activate', () => {
      if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
  })

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
  })

  